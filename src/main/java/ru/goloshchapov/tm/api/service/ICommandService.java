package ru.goloshchapov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.goloshchapov.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.List;

public interface ICommandService {

    @NotNull List<AbstractCommand> getCommandList();

    @NotNull Collection<String> getListCommandNames();

    @NotNull Collection<String> getListCommandArgs();
}
