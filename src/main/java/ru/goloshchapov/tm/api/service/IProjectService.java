package ru.goloshchapov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.goloshchapov.tm.api.IBusinessService;
import ru.goloshchapov.tm.model.Project;

public interface IProjectService extends IBusinessService<Project> {

    @NotNull Project add(String userId, String name, String description);

}
