package ru.goloshchapov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.service.ServiceLocator;
import ru.goloshchapov.tm.enumerated.Role;

public abstract class AbstractCommand {

    @Nullable
    protected ServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable public Role[] roles() { return null; }

    @Nullable public abstract String arg();

    @Nullable public abstract String name();

    @Nullable public abstract String description();

    public abstract void execute();

}
