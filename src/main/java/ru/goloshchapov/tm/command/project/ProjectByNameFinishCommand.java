package ru.goloshchapov.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.exception.entity.ProjectNotFoundException;
import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class ProjectByNameFinishCommand extends AbstractProjectCommand{

    @NotNull public static final String NAME = "project-finish-by-name";

    @NotNull public static final String DESCRIPTION ="Finish project by name";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectService().finishOneByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
    }
}
