package ru.goloshchapov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.goloshchapov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ShowCommandsCommand extends AbstractCommand {

    @NotNull public static final String ARGUMENT = "-cmd";

    @NotNull public static final String NAME = "commands";

    @NotNull public static final String DESCRIPTION = "Show program commands";

    @NotNull
    @Override
    public String arg() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final Collection<String> commands = serviceLocator.getCommandService().getListCommandNames();
        for (@NotNull final String cmd:commands) System.out.println(cmd);
    }
}
